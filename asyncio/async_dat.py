import aiofiles  # -> for work with files in async way
import asyncio  # Module for run async code
import time  # -> for calculation the time of a function call
from random import choice, uniform   # -> for making data with random choice and random float in diapason.


# Creating async func for writing .dat files:
async def create_data(file_count):
    start = time.time()
    math_choice_list = ['1', '2', '3']
    for file in range(file_count):
        math_operation = choice(math_choice_list)
        first_num_var = uniform(0, 1000)
        second_num_var = uniform(-1000, 1000)
        async with aiofiles.open(f'dat_files/in_{file}.dat', 'w') as dat_file:
            await dat_file.write(math_operation + '\n')
            await dat_file.write(str(first_num_var) + ' ' + str(second_num_var))
    print(time.time() - start)


if __name__ == "__main__":
    asyncio.run(create_data(1500))  # -> python 3.7 can do it like this
