import aiofiles  # -> for work with files in async way
import asyncio  # Module for run async code
import time


# Make a function to write the results over the operands from the created files into one
async def result_dat(n_file):
    """
    Я пытался сделать с помощью async with aiofiles.open, но мне выдает такую ошибку
    AttributeError: 'generator' object has no attribute 'strip',
    к сожалению гугл не помог и пришлось вернуться просто к with в первой части функции
    """

    start = time.time()
    TOTAL_RESULT = 0
    with open(f'dat_files/in_{n_file}.dat') as dat_file:
        math_choice = dat_file.readline().strip()
        first_num_var, second_num_var = map(float, dat_file.readline().split())
        if math_choice == '1':
            result = first_num_var + second_num_var
            TOTAL_RESULT += result
        if math_choice == '2':
            result = first_num_var * second_num_var
            TOTAL_RESULT += result
        if math_choice == '3':
            result = first_num_var * first_num_var + second_num_var * second_num_var  # var * var cause **2 greedy
            TOTAL_RESULT += result
        async with aiofiles.open('./result_file.dat', 'a') as result_file:
            await result_file.write(f'Dat file № #{n_file} --> Result: {result}\n')
            async with aiofiles.open('./total_result_file.dat', 'w') as total_result_file:
                await total_result_file.write(f'Final Result {TOTAL_RESULT}')

        print(time.time() - start)


async def main_result():
    tasks = [asyncio.create_task(result_dat(num_file)) for num_file in range(1500)]
    # for num_file in range(1500):
    #     tasks.append(asyncio.create_task(result_dat(num_file)))
    for task in tasks:
        await task


if __name__ == "__main__":
    asyncio.run(main_result())


