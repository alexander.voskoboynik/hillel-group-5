from rest_framework import serializers  # base class for serializers
from core.models import Group, Student, Teacher


# Classes for api Group, Teacher, Student models:
class TeacherSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)

    class Meta:
        model = Teacher
        fields = "__all__"


class GroupSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    group_lead = serializers.SlugRelatedField(queryset=Teacher.objects.all(), slug_field='name', many=True)

    class Meta:
        model = Group
        fields = "__all__"


class StudentSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    course = serializers.SlugRelatedField(queryset=Group.objects.all(), slug_field='course', many=True)

    class Meta:
        model = Student
        fields = "__all__"