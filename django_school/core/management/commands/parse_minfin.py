from django.core.management.base import BaseCommand
import requests
import lxml.html as lh
from core.models import ExchangeCurrency


class Command(BaseCommand):

    def handle(self, *args, **options):
        response = requests.get("https://minfin.com.ua/currency/")
        if response.status_code == 200:
            tree = lh.fromstring(response.content)
            currency_rate = []
            # print(tree.xpath('/html/body/div[3]/div[3]/div/div[1]/div[1]/div[1]/div/table/tbody')[0])
            tree_x_path = tree.xpath('/html/body/main/div/div/div[1]/div/section[2]/div/table[1]/tbody')[0]
            # print(tree_x_path.text_content())
            for tr in tree_x_path[0:3]:
                # currency_rate.append(tr.text_content().split())
                # print(currency_rate)
                for field in tr:
                    currency_rate.append(field.text_content().split())
                    # print(currency_rate)
            ExchangeCurrency.objects.create(
                source='MinFin',
                dollar=f"USD: {currency_rate[1][0]}₴ / {currency_rate[1][4]}₴\n",
                euro=f"EUR: {currency_rate[5][0]}₴ / {currency_rate[5][4]}₴\n",
                ruble=f"RUB: {currency_rate[-3][0]}₴ / {currency_rate[-3][4]}₴",
            )