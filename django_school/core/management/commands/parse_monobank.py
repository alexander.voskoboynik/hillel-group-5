from django.core.management.base import BaseCommand
import requests
from core.models import ExchangeCurrency


class Command(BaseCommand):

    def handle(self, *args, **options):
        # Курс по Монобанк
        response = requests.get("https://api.monobank.ua/bank/currency")
        if response.status_code == 200:
            tree = response.json()
            # print(tree)
            ExchangeCurrency.objects.create(
                source="Monobank",
                dollar=f'USD: {tree[0]["rateBuy"]}₴ / {tree[0]["rateSell"]}₴\n',
                euro=f'EUR: {tree[1]["rateBuy"]}₴ / {tree[1]["rateSell"]}₴\n',
                ruble=f'RUB: {tree[2]["rateBuy"]}₴ / {tree[2]["rateSell"]}₴',
            )