from django.core.management.base import BaseCommand
import requests
import lxml.html as lh
from core.models import ExchangeCurrency


class Command(BaseCommand):

    def handle(self, *args, **options):
        response = requests.get("https://privatbank.ua/")
        if response.status_code == 200:
            tree = lh.fromstring(response.content)
            currency_rate = []
            tree_x_path = tree.xpath('//*[@id="widget"]/div[1]/article/div[2]/div/table/tbody')[0]
            # print(tree_x_path.text_content())
            for tr in tree_x_path:
                currency_rate.append(tr.text_content().split())
            ExchangeCurrency.objects.create(
                source='Privat',
                dollar=f"USD: {currency_rate[0][2]}₴ / {currency_rate[0][3]}₴\n",
                euro=f"EUR: {currency_rate[1][2]}₴ / {currency_rate[1][3]}₴\n",
                ruble=f"RUB: {currency_rate[2][2]}₴ / {currency_rate[2][3]}₴",
            )
