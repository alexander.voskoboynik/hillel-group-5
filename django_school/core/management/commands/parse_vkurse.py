from django.core.management.base import BaseCommand
import requests
from core.models import ExchangeCurrency


class Command(BaseCommand):

    def handle(self, *args, **options):
        # Курс http://vkurse.dp.ua/
        response = requests.get("http://vkurse.dp.ua/course.json")
        if response.status_code == 200:
            tree = response.json()
            print(tree)
            ExchangeCurrency.objects.create(
                source="http://vkurse.dp.ua/",
                dollar=f"USD: {tree['Dollar']['buy']}₴ / {tree['Dollar']['sale']}₴\n",
                euro=f"EUR: {tree['Euro']['buy']}₴ / {tree['Euro']['sale']}₴\n",
                ruble=f"RUB: {tree['Rub']['buy']}₴ / {tree['Rub']['sale']}₴",
            )