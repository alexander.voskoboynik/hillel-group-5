# Generated by Django 3.1.7 on 2021-04-28 04:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0005_auto_20210428_0433'),
    ]

    operations = [
        migrations.AlterField(
            model_name='logger',
            name='log_time',
            field=models.DateTimeField(),
        ),
    ]
