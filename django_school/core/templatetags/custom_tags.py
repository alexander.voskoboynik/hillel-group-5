from re import findall
from django import template
from random import choice, choices
from core.models import Teacher

register = template.Library()


# Custom tags:
# Custom tag for take five random teachers
@register.simple_tag
def random_five_teachers():
    # teachers = Teacher.objects.all()
    # random_teachers = []
    # for elem in range(5):
    #     random_teachers.append(choice(teachers).name)
    # return random_teachers
    return Teacher.objects.all().order_by('?')[0:5]


# Custom filters:
# filter for outputs all even numbers from an array:
@register.filter
def even_numbers_from_array(array):
    return [num for num in array if num % 2 == 0]


# filter for return words quantity in sentence
@register.filter
def words_quantity_in_string(sentence: str):
    look_for_words = r'\w+'
    sentence_list = findall(look_for_words, sentence)
    return len(sentence_list)
