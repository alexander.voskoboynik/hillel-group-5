from flask import Flask, render_template, request
from faker import Faker
import requests
app = Flask(__name__)


@app.route('/')
def show_requirements_file():
    with open('requirements.txt') as requirement_venv_file:
        requirement_packages_list = requirement_venv_file.readlines()
        requirement_packages_list = [req.strip('\n') for req in requirement_packages_list]
        return render_template('vs_requirements.html', library=requirement_packages_list)


@app.route('/faker')
def random_users_emails():
    fake = Faker(['it_IT', 'en_US', 'ja_JP'])
    fake_names_and_email_list = [fake.name() + ' -- ' + fake.email() for i in range(100)]
    return render_template('fake_names.html', fakelibrary=fake_names_and_email_list)


@app.route('/astronauts')
def astronauts():
    astronauts_req = requests.get('http://api.open-notify.org/astros.json')
    current_astronauts = astronauts_req.json()
    return render_template('astronauts.html', **current_astronauts)


if __name__ == '__main__':
    app.run(
        host='127.0.0.1',
        port=5000,
        debug=True
    )
