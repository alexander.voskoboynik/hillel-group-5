# Must have imports
import sqlite3
from flask import Flask, render_template, request, redirect
app = Flask(__name__)


# Connection to DB func:
def execute_query(query):
    with sqlite3.connect('vosk_books.db') as connection:
        cursor = connection.cursor()
        cursor.execute(query)
        return cursor.fetchall()


# Func for getting value from DB by table_name, id to replace int parameters
def get_by_id(table, record_id):
    return execute_query(f"SELECT * FROM {table} WHERE id={record_id}")[0]


# Home Page: Full library list
@app.route('/')
def book_func():
    books = execute_query("SELECT * FROM books")
    return render_template('books.html',
                           books=books, get_by_id=get_by_id)


# Sort by year page:
@app.route('/year/<int:pk>/')
def filter_by_year(pk):
    books = execute_query(f"SELECT * FROM books WHERE year={pk}")
    return render_template('books.html',
                           books=books, get_by_id=get_by_id)


# Sort by author page:
@app.route('/author/<int:pk>/')
def filter_by_author(pk):
    books = execute_query(f"SELECT * FROM books WHERE author={pk}")
    return render_template('books.html',
                           books=books, get_by_id=get_by_id)


# Sort by genre page:
@app.route('/genre/<int:pk>/')
def filter_by_genre(pk):
    books = execute_query(f"SELECT * FROM books WHERE genre={pk}")
    return render_template('books.html',
                           books=books, get_by_id=get_by_id)


# Adding new author to database(DB):
@app.route('/create/author/', methods=['GET', 'POST'])
def create_author():
    if request.method == 'POST':
        execute_query(
            f"INSERT INTO author (name) VALUES ('{request.form['name']}')"
        )
        return redirect('/')
    return render_template('create_author.html')


# Add new genre to DB:
@app.route('/create/genre/', methods=['GET', 'POST'])
def create_genre():
    if request.method == 'POST':
        execute_query(
            f"INSERT INTO genre (name) VALUES ('{request.form['name']}')"
        )
        return redirect('/')
    return render_template('create_genre.html')


# Add new book to DB (работает только если знать id автора и жанра):
@app.route('/create/book/', methods=['GET', 'POST'])
def add_book():
    if request.method == 'POST':
        execute_query(
            f"INSERT INTO books (name, year, author, genre) VALUES ('{request.form['name']}', "
            f"{request.form['year']}, {request.form['author']}, {request.form['genre']})"
        )
        return redirect('/')
    return render_template('create_book.html')


# Update book in DB:
@app.route('/update/book/<int:pk>/', methods=['GET', 'POST'])
def update_book(pk):
    if request.method == 'POST':
        execute_query(
            f"UPDATE books SET (name, year, author, genre) = ('{request.form['name']}', "
            f"{request.form['year']}, {request.form['author']}, {request.form['genre']})  WHERE id = {pk}"
        )
        return redirect('/')
    return render_template('update_book.html')


if __name__ == '__main__':
    app.run(
        host='127.0.0.1',
        port=5000,
        debug=True
    )
