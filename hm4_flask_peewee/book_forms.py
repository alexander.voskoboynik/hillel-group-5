import wtforms
from book_models import Author, Genre, Books


# Form for book (create, update) in Books table
class BookForm(wtforms.Form):
    name = wtforms.StringField(validators=[wtforms.validators.DataRequired()])
    year = wtforms.IntegerField(validators=[wtforms.validators.DataRequired()])
    author = wtforms.SelectField(
        choices=[(x.id, x.name) for x in Author.select()]
    )
    genre = wtforms.SelectField(
        choices=[(x.id, x.name) for x in Genre.select()]
    )

    def save(self):
        Books.create(name=self.name.data,
                     year=self.year.data,
                     author=self.author.data,
                     genre=self.genre.data)

    def update(self, book_id):
        Books.update(name=self.name.data,
                     year=self.year.data,
                     author=self.author.data,
                     genre=self.genre.data).where(Books.id == book_id).execute()


# Form for author create in Author table:
class AuthorForm(wtforms.Form):
    name = wtforms.StringField(validators=[wtforms.validators.DataRequired()])
    
    def save(self):
        Author.create(name=self.name.data)


# Form for genre create in Genre table:
class GenreForm(wtforms.Form):
    name = wtforms.StringField(validators=[wtforms.validators.DataRequired()])
    
    def save(self):
        Genre.create(name=self.name.data)

