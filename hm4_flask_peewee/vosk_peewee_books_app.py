# Must have imports for task:
from flask import Flask, render_template, request, redirect
from book_models import Author, Genre, Books
from book_forms import BookForm, AuthorForm, GenreForm

app = Flask(__name__)


# Home Page: Full library list
@app.route('/')
def book_func():
    books_lib = Books.select().join_from(Books, Author).join_from(Books, Genre)
    return render_template('pw_books.html', books=books_lib)


# Upgrade filter by the author page:
@app.route('/author/<int:pk>/')
def filter_by_author(pk):
    books_lib = Books.select().where(Books.author == pk)
    return render_template('pw_books.html', books=books_lib)


# Upgrade filter by the genre page:
@app.route('/genre/<int:pk>/')
def filter_by_genre(pk):
    books_lib = Books.select().where(Books.genre == pk)
    return render_template('pw_books.html', books=books_lib)


# Upgrade filter by the year page:
@app.route('/year/<int:pk>/')
def filter_by_year(pk):
    books_lib = Books.select().where(Books.year == pk)
    return render_template('pw_books.html', books=books_lib)


# Adding new author to database(DB):
@app.route('/create/author/', methods=['GET', 'POST'])
def create_author():
    form = AuthorForm()
    if request.method == 'POST':
        form = AuthorForm(formdata=request.form)
        if form.validate():
            form.save()
        return redirect('/')
    return render_template('pw_create_author.html', form=form)


# Adding new genre to database(DB):
@app.route('/create/genre/', methods=['GET', 'POST'])
def create_genre():
    form = GenreForm()
    if request.method == 'POST':
        form = GenreForm(formdata=request.form)
        if form.validate():
            form.save()
        return redirect('/')
    return render_template('pw_create_genre.html', form=form)


# Add new book to DB:
@app.route('/create/book/', methods=['GET', 'POST'])
def create_book():
    form = BookForm()
    if request.method == 'POST':
        form = BookForm(formdata=request.form)
        if form.validate():
            form.save()
        return redirect('/')
    return render_template('pw_create_book.html', form=form)


# Update book in DB:
@app.route('/update/book/<int:pk>/', methods=['GET', 'POST'])
def update_book(pk):
    form = BookForm(obj=Books.select().where(Books.id == pk).get())
    if request.method == 'POST':
        form = BookForm(formdata=request.form)
        if form.validate():
            form.update(pk)
        return redirect('/')
    return render_template('pw_update_book.html', form=form)


if __name__ == '__main__':
    app.run(
        host='127.0.0.1',
        port=5000,
        debug=True
    )
