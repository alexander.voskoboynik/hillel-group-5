def parse(query: str) -> dict:
    processed_query = (query.replace('&', '?').replace('=', '?').split('?'))[1:]
    result_dict = {k: v for (k, v) in zip(processed_query[::2], processed_query[1::2])}
    return result_dict


if __name__ == '__main__':
    assert parse('https://example.com/path/to/page?name=ferret&color=purple') == {'name': 'ferret', 'color': 'purple'}
    assert parse('https://example.com/path/to/page?name=ferret&color=purple&') == {'name': 'ferret', 'color': 'purple'}
    assert parse('http://example.com/') == {}
    assert parse('http://example.com/?') == {}
    assert parse('http://example.com/?name=Dima') == {'name': 'Dima'}


def parse_cookie(query: str) -> dict:
    if '=' in query:
        processed_query = query.strip(';').split(';')
        result_name_and_age_dict = {}
        for i in processed_query:
            result_name_and_age_dict[i.split('=', 1)[0]] = i.split('=', 1)[1]
        return result_name_and_age_dict
    else:
        return {}


if __name__ == '__main__':
    assert parse_cookie('name=Dima;') == {'name': 'Dima'}
    assert parse_cookie('') == {}
    assert parse_cookie('name=Dima;age=28;') == {'name': 'Dima', 'age': '28'}
    assert parse_cookie('name=Dima=User;age=28;') == {'name': 'Dima=User', 'age': '28'}

# for me
